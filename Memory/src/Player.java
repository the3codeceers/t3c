
public class Player {
	
	String playerName;
	int score;
	
	
	public Player(String playerName) {
		super();
		this.playerName = playerName;
		this.score = 0;
	}
	
	public String getPlayerName() {
		return playerName;
	}
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	
	

}
