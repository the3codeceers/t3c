import java.awt.Point;
import java.util.Vector;


public class Board implements IBoard {
	LlistaOrdreAleatori<Square> board;
	Vector<Integer> scoreBoard;
	int numPairs;
	int numPlayers;
	
	public Board(int numPairs, int numPlayers) {
		super();
		this.numPairs = numPairs;
		this.numPlayers = numPlayers;
		scoreBoard = new Vector<Integer>();
		board = new LlistaOrdreAleatori<Square>();
		for (int i = 0; i<numPlayers; i++){
			scoreBoard.add(0);
		}
		for (int i = 0; i<numPairs; i++ ){
			board.add(new Square<Integer>(i,i));
			board.add(new Square<Integer>(i,i));
		}
		
		board.mix();
	}

	public LlistaOrdreAleatori<Square> getBoard() {
		return board;
	}

	public void setScoreBoard(int numPlayer, int plusPoints) {
		scoreBoard.set(numPlayer, scoreBoard.get(numPlayer)+plusPoints);
	}

	public Vector<Integer> getScoreBoard() {
		return scoreBoard;
	}

	public int getNumPairs() {
		return numPairs;
	}

	@Override
	public Point getSizeBoard() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
	
	

}
