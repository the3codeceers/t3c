import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Window extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4648172894076113183L;

	private int numPlayers;

	private Vector<String> names = new Vector<String>();

	private int numCouples;

	private IBoard board;

	private JPanel wind;

	private LlistaOrdreAleatori<Square> squares;

	private JButton ini;
	
	private boolean withLabel = true;
	
	
	
	
	
	
	
	
	
	
	
	
	
	public Window() {
		super("Memory");

		numPlayers = 2;

		askNumCouples();

		askPlayers();
		this.setLayout(new BorderLayout());
		wind = new JPanel();
		wind.setLayout(new GridLayout(numCouples, numCouples));

		this.add(wind, BorderLayout.CENTER);
		ini = new JButton("Iniciar");
		ini.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				withLabel = false;
				draw();
			}
		});
		
		this.add(ini, BorderLayout.SOUTH);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setSize(600, 600);
		this.setVisible(true);
	}

	private void askPlayers() {
		for (int num = 1; num <= numPlayers; num++) {
			names.add(JOptionPane.showInputDialog("Introdueix nom de jugador:"));
		}
	}

	public void setBoard(IBoard tauler) {
		board = tauler;
	}

	public void draw() {
		squares = board.getBoard();
		wind.removeAll();
		String label = "";
		for (int i =0; i<squares.size();i++) {
			if (withLabel) label = ""+squares.get(i).getId();
			else label=" a";
			wind.add(new JButton(label), i);
		}
		
		this.repaint();
	}

	private void askNumCouples() {
		do {
			numCouples = -1;
			String couples = (JOptionPane.showInputDialog("Nombre de parelles"));
			try {
				numCouples = Integer.parseInt(couples);
			} catch (NumberFormatException e) {

			}
		} while (numCouples > 50 || numCouples < 0);
	}

	public int getNumPlayers() {
		return numPlayers;
	}

	public Vector<String> getNames() {
		return names;
	}

	public int getNumCouples() {
		return numCouples;
	}
	
}
