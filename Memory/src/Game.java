import java.util.Vector;


public class Game implements IGame{
	
	private Board board;
	private int numPlayers;
	private int numPairs;
	private int turn;
	private Vector <Player> players;
	private Window win;
	
	public static void main(String args[])
	{
		Game g = new Game();
	}
	public Game()
	{
		players = new Vector<Player>();		
		win = new Window();
		
		numPairs = win.getNumCouples();
		numPlayers = win.getNumPlayers();
		
		Vector<String> namesAux = win.getNames();
		for(int i = 0; i < numPlayers; i++)
		{
			players.add(new Player(namesAux.get(i)));
		}
		
		board = new Board(numPairs, numPlayers);
		this.turn = initTurn();
		win.setBoard(board);
		win.draw();
	}
	@Override
	public int numPlayers() {

		return players.size();
	}

	@Override
	public int getTurn() {
		return turn;
	}

	@Override
	public IBoard getBoard() {
		return null;
	}

	@Override
	public boolean checkCards(int position1, int position2) {
		return false;
	}
	private int initTurn() {
		double random = Math.random();
		return (int)random*players.size();
	}
	@Override
	public void setScoreBoard(int player) {
		board.setScoreBoard(player, 10);
	}
	@Override
	public Vector<Integer> getScoreBoard() {
		
		return board.getScoreBoard();
	}

}
