import java.util.Vector;



public interface IGame {
	int numPlayers();
	int getTurn();
	IBoard getBoard();
	boolean checkCards(int position1, int position2);
	void setScoreBoard(int player);
	Vector<Integer> getScoreBoard();
}
