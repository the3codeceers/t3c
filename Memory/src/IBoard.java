import java.awt.Point;
import java.util.Vector;


public interface IBoard {
	public Vector<Integer> getScoreBoard();
	public Point getSizeBoard();
	public LlistaOrdreAleatori<Square> getBoard();
}
