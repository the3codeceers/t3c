
public class Square <E> {

	int id;
	E source;
	
	public Square(int id, E source) {
		super();
		this.id = id;
		this.source = source;
		this.checked = false;
	}
	boolean checked;
	
	public E getSource() {
		return source;
	}
	public void setSource(E source) {
		this.source = source;
	}
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	public int getId() {
		return id;
	}
	
	
	
}
